package com.ruoyi.web.controller.uploadmoment;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 所有get请求页面管理
 * @author lcl
 * @date 2021-03-30
 */
@Controller
@RequestMapping("/uploadmoment")
public class MvMainController{

    private String prefix = "uploadmoment";

    /**
     * 主页
     * @return
     */
    @GetMapping("/index.html")
    public String mvmainIndex() {
        return prefix + "/index";
    }

    /**
     * 上传下载页
     * @return
     */
    @GetMapping("/Upload_Video.html")
    public String UploadVideo() {
        return prefix + "/Upload_Video";
    }

    /**
     * 个人中心页
     * @return
     */
    @GetMapping("/singlechannelabout")
    public String SingleChannelAbout() {
        return prefix + "/Single_Channel_About";
    }
    /**
     * 登陆页
     * @return
     */
    @GetMapping("/login")
    public String login() {
        return prefix + "/login";
    }
    /**
     * 注册页
     * @return
     */
    @GetMapping("/signup")
    public String signup() {
        return prefix + "/signup";
    }
}
