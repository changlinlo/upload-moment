package com.ruoyi.web.controller.uploadmoment;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.uploadmoment.domain.LclVideo;
import com.ruoyi.uploadmoment.service.ILclVideoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 视频信息Controller
 * 
 * @author lcl
 * @date 2022-01-11
 */
@Controller
@RequestMapping("/uploadmoment/video")
public class LclVideoController extends BaseController
{
    private String prefix = "uploadmoment/video";

    @Autowired
    private ILclVideoService lclVideoService;

    @RequiresPermissions("uploadmoment:video:view")
    @GetMapping()
    public String video()
    {
        return prefix + "/video";
    }

    /**
     * 查询视频信息列表
     */
    @RequiresPermissions("uploadmoment:video:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LclVideo lclVideo)
    {
        startPage();
        List<LclVideo> list = lclVideoService.selectLclVideoList(lclVideo);
        return getDataTable(list);
    }

    /**
     * 导出视频信息列表
     */
    @RequiresPermissions("uploadmoment:video:export")
    @Log(title = "视频信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LclVideo lclVideo)
    {
        List<LclVideo> list = lclVideoService.selectLclVideoList(lclVideo);
        ExcelUtil<LclVideo> util = new ExcelUtil<LclVideo>(LclVideo.class);
        return util.exportExcel(list, "视频信息数据");
    }

    /**
     * 新增视频信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存视频信息
     */
    @RequiresPermissions("uploadmoment:video:add")
    @Log(title = "视频信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LclVideo lclVideo)
    {
        return toAjax(lclVideoService.insertLclVideo(lclVideo));
    }

    /**
     * 修改视频信息
     */
    @RequiresPermissions("uploadmoment:video:edit")
    @GetMapping("/edit/{videoId}")
    public String edit(@PathVariable("videoId") String videoId, ModelMap mmap)
    {
        LclVideo lclVideo = lclVideoService.selectLclVideoByVideoId(videoId);
        mmap.put("lclVideo", lclVideo);
        return prefix + "/edit";
    }

    /**
     * 修改保存视频信息
     */
    @RequiresPermissions("uploadmoment:video:edit")
    @Log(title = "视频信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LclVideo lclVideo)
    {
        return toAjax(lclVideoService.updateLclVideo(lclVideo));
    }

    /**
     * 删除视频信息
     */
    @RequiresPermissions("uploadmoment:video:remove")
    @Log(title = "视频信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(lclVideoService.deleteLclVideoByVideoIds(ids));
    }
}
