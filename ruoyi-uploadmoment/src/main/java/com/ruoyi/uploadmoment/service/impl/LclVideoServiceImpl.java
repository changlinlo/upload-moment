package com.ruoyi.uploadmoment.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.uploadmoment.domain.LclVideo;
import com.ruoyi.uploadmoment.mapper.LclVideoMapper;
import com.ruoyi.uploadmoment.service.ILclVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * 视频信息Service业务层处理
 * 
 * @author lcl
 * @date 2022-01-11
 */
@Service
public class LclVideoServiceImpl implements ILclVideoService 
{
    @Autowired
    private LclVideoMapper lclVideoMapper;

    /**
     * 查询视频信息
     * 
     * @param videoId 视频信息主键
     * @return 视频信息
     */
    @Override
    public LclVideo selectLclVideoByVideoId(String videoId)
    {
        return lclVideoMapper.selectLclVideoByVideoId(videoId);
    }

    /**
     * 查询视频信息列表
     * 
     * @param lclVideo 视频信息
     * @return 视频信息
     */
    @Override
    public List<LclVideo> selectLclVideoList(LclVideo lclVideo)
    {
        return lclVideoMapper.selectLclVideoList(lclVideo);
    }

    /**
     * 新增视频信息
     * 
     * @param lclVideo 视频信息
     * @return 结果
     */
    @Override
    public int insertLclVideo(LclVideo lclVideo)
    {
        lclVideo.setVideoId(UUID.randomUUID().toString());
        return lclVideoMapper.insertLclVideo(lclVideo);
    }

    /**
     * 修改视频信息
     * 
     * @param lclVideo 视频信息
     * @return 结果
     */
    @Override
    public int updateLclVideo(LclVideo lclVideo)
    {
        return lclVideoMapper.updateLclVideo(lclVideo);
    }

    /**
     * 批量删除视频信息
     * 
     * @param videoIds 需要删除的视频信息主键
     * @return 结果
     */
    @Override
    public int deleteLclVideoByVideoIds(String videoIds)
    {
        return lclVideoMapper.deleteLclVideoByVideoIds(Convert.toStrArray(videoIds));
    }

    /**
     * 删除视频信息信息
     * 
     * @param videoId 视频信息主键
     * @return 结果
     */
    @Override
    public int deleteLclVideoByVideoId(String videoId)
    {
        return lclVideoMapper.deleteLclVideoByVideoId(videoId);
    }
}
