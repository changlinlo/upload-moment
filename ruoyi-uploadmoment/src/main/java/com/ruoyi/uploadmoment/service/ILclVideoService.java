package com.ruoyi.uploadmoment.service;

import java.util.List;
import com.ruoyi.uploadmoment.domain.LclVideo;

/**
 * 视频信息Service接口
 * 
 * @author lcl
 * @date 2022-01-11
 */
public interface ILclVideoService 
{
    /**
     * 查询视频信息
     * 
     * @param videoId 视频信息主键
     * @return 视频信息
     */
    public LclVideo selectLclVideoByVideoId(String videoId);

    /**
     * 查询视频信息列表
     * 
     * @param lclVideo 视频信息
     * @return 视频信息集合
     */
    public List<LclVideo> selectLclVideoList(LclVideo lclVideo);

    /**
     * 新增视频信息
     * 
     * @param lclVideo 视频信息
     * @return 结果
     */
    public int insertLclVideo(LclVideo lclVideo);

    /**
     * 修改视频信息
     * 
     * @param lclVideo 视频信息
     * @return 结果
     */
    public int updateLclVideo(LclVideo lclVideo);

    /**
     * 批量删除视频信息
     * 
     * @param videoIds 需要删除的视频信息主键集合
     * @return 结果
     */
    public int deleteLclVideoByVideoIds(String videoIds);

    /**
     * 删除视频信息信息
     * 
     * @param videoId 视频信息主键
     * @return 结果
     */
    public int deleteLclVideoByVideoId(String videoId);
}
