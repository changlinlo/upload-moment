package com.ruoyi.uploadmoment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 视频信息对象 lcl_video
 * 
 * @author lcl
 * @date 2022-01-11
 */
public class LclVideo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 视频主键 */
    private String videoId;

    /** 视频标题 */
    @Excel(name = "视频标题")
    private String videoTitle;

    /** 视频路径 */
    @Excel(name = "视频路径")
    private String videoPath;

    /** 视频图片路径 */
    @Excel(name = "视频图片路径")
    private String videoImagePath;

    /** 视频播放次数 */
    @Excel(name = "视频播放次数")
    private Long videoWatchCount;

    /** 视频类型 */
    @Excel(name = "视频类型")
    private Integer videoType;

    /** 视频上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "视频上传时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date videoUploadTime;

    /** 视频状态 */
    @Excel(name = "视频状态")
    private Integer videoState;

    /** 视频简介说明 */
    @Excel(name = "视频简介说明")
    private String videoExplain;

    /** 视频出处 */
    @Excel(name = "视频出处")
    private String isDiy;

    /** 视频所属用户 */
    @Excel(name = "视频所属用户")
    private Long userId;

    public void setVideoId(String videoId) 
    {
        this.videoId = videoId;
    }

    public String getVideoId() 
    {
        return videoId;
    }
    public void setVideoTitle(String videoTitle) 
    {
        this.videoTitle = videoTitle;
    }

    public String getVideoTitle() 
    {
        return videoTitle;
    }
    public void setVideoPath(String videoPath) 
    {
        this.videoPath = videoPath;
    }

    public String getVideoPath() 
    {
        return videoPath;
    }
    public void setVideoImagePath(String videoImagePath) 
    {
        this.videoImagePath = videoImagePath;
    }

    public String getVideoImagePath() 
    {
        return videoImagePath;
    }
    public void setVideoWatchCount(Long videoWatchCount) 
    {
        this.videoWatchCount = videoWatchCount;
    }

    public Long getVideoWatchCount() 
    {
        return videoWatchCount;
    }
    public void setVideoType(Integer videoType) 
    {
        this.videoType = videoType;
    }

    public Integer getVideoType() 
    {
        return videoType;
    }
    public void setVideoUploadTime(Date videoUploadTime) 
    {
        this.videoUploadTime = videoUploadTime;
    }

    public Date getVideoUploadTime() 
    {
        return videoUploadTime;
    }
    public void setVideoState(Integer videoState) 
    {
        this.videoState = videoState;
    }

    public Integer getVideoState() 
    {
        return videoState;
    }
    public void setVideoExplain(String videoExplain) 
    {
        this.videoExplain = videoExplain;
    }

    public String getVideoExplain() 
    {
        return videoExplain;
    }
    public void setIsDiy(String isDiy) 
    {
        this.isDiy = isDiy;
    }

    public String getIsDiy() 
    {
        return isDiy;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("videoId", getVideoId())
            .append("videoTitle", getVideoTitle())
            .append("videoPath", getVideoPath())
            .append("videoImagePath", getVideoImagePath())
            .append("videoWatchCount", getVideoWatchCount())
            .append("videoType", getVideoType())
            .append("videoUploadTime", getVideoUploadTime())
            .append("videoState", getVideoState())
            .append("videoExplain", getVideoExplain())
            .append("isDiy", getIsDiy())
            .append("userId", getUserId())
            .toString();
    }
}
